#include "Board.hpp"
#include "report.hpp"

#include <fstream>
#include <iostream>
#include <vector>

class ReportStdout : public Reportable {

public:
    void report(const Itemable& item) override {
            for (const std::string & item : item.getItems())
                std::cout << item << std::endl;
            std::cout << std::endl;
        }
};