#include "Board.hpp"
#include "report.hpp"

#include <fstream>
#include <iostream>
#include <vector>

class ReportFile : public Reportable {
private:
    std::ofstream _ofs;

public:
    ReportFile (const std::string filename) : _ofs(filename){}

    void report(const Itemable& item) override{
                for (const std::string & item : item.getItems())
                    _ofs << item << std::endl;
                _ofs << std::endl;
            }
};