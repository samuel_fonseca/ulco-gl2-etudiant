#pragma once

#include <string>

class Title {
    public:
        virtual std::string getTitle() const = 0;
};
