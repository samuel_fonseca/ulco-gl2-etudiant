#include "Board.hpp"

#include <fstream>
#include <iostream>
#include <vector>

class NumBoard: public Board {
private:
    int num;
public:
    NumBoard () : num(1){}

    void addNum(const std::string & t) {
               std::string tmp = std::to_string(num)+ "." + t;
               add(tmp);
               num++;
            }
};