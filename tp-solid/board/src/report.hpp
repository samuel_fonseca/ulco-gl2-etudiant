#pragma once
#include "Board.hpp"

#include <string>
#include <vector>

class Reportable {
    public:
        virtual void report(const Itemable& item) = 0;
};