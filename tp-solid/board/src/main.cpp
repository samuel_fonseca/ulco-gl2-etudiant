
#include "Board.hpp"
#include "ReportStdout.hpp"
#include "ReportFile.hpp"
#include "Numboard.hpp"
#include "report.hpp"

void testBoard(Board & b, Reportable &report) {
    std::cout << b.getTitle() << std::endl;
    b.add("item 1");
    b.add("item 2");

    report.report(b);
}

void testNumBoard(NumBoard & b, Reportable &report) {
    std::cout << b.getTitle() << std::endl;
    b.addNum("item 1");
    b.addNum("item 2");

    report.report(b);
}

int main() {

    Board b1;
    ReportStdout r;
    testBoard(b1, r);

    NumBoard b2;
    ReportStdout r2;
    testNumBoard(b2, r2);

    return 0;
}

