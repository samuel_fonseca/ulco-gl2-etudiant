with import <nixpkgs> {};

let
  masuperlib-src = fetchTarball "https://gitlab.com/samuel_fonseca/masuperlib/-/archive/master/masuperlib-master.tar.gz";
  masuperlib = callPackage masuperlib-src {};

in clangStdenv.mkDerivation {
    name = "masuperpp";
    src = ./.;
    nativeBuildInputs = [ cmake ];
    buildInputs = [ masuperlib ];
}

