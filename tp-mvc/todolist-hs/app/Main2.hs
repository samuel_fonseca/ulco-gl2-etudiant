import Board
import View2

import Text.Read (readMaybe)

loop :: Board -> IO ()
loop b = do
    putStrLn ""
    printBoard b

    a <- getLine
    case words a of
        ["quit"] -> putStr "Au revoir"
        ("add":xs) -> 
            let
                (i, b2) = loop addTodo (unwords xs) b 
            in loop b2
        ["done",x] -> loop $ toDone (read x) b
        _ -> loop b
    
main :: IO ()
main = do
    let (i,b) = addTodo "Premiere tache" newBoard
    loop b