module Board where

import Data.List (partition)
import Task

data Board = Board
    { _boardId :: Int
    , _boardTodo :: [Task]
    , _boardDone :: [Task]
    }

newBoard :: Board
newBoard = Board 1 [] []

addTodo :: String -> Board -> (Int, Board)
addTodo str (Board i ts ds) = 
    let new_task = Task i str
        board = Board (i+1) (ts++[new_task]) ds
    in (i, board)

    
    

toDone :: Int -> Board -> Board
toDone i0 (Board _i ts ds) =
    let (ts0, ts1) = partition ((==i0) . _taskId) ts
    in Board i0 ts1 (ts0 ++ ds)
    

