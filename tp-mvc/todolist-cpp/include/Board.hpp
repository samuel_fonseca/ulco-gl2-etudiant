#include "task.hpp"

#include <list>
#include <algorithm>

class Board
{
    public:
    int BoardId;
    std::list<Task> boardTodo;
    std::list<Task> boardDone;

    Board(int Id): BoardId(Id){}

    void addTask(std::string str);

    void toDone(int index);
};