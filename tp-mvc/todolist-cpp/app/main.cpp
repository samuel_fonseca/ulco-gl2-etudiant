#include <todolist-cpp/todolist-cpp.hpp>
#include "Board.hpp"

#include <iostream>

void aff_todoList(Board b){
    std::cout<< "TODO :"<< std::endl;
    for (Task n : b.boardTodo) {
        std::cout << n.Id << ". " << n.Name << std::endl;
    }
}

void aff_DoneList(Board b){
    std::cout<< "DONE :"<< std::endl;    
    for (Task n : b.boardDone) {
        std::cout << n.Id << ". " << n.Name << std::endl;
    }
}

int main() {

    Board b = Board(1);
    //b.addTask("salut");
    
    std::string request = "";
    
    while(request != "quit"){
        aff_todoList(b);
        aff_DoneList(b);
        std::cin >> request;
        
        if(request.find("add")){
            std::string str = request.substr(4);
            b.addTask(str);
        }

        if(request.find("done")){
            //std::string str = request.substr(4);
            //b.toDone();
        }

    }
    
    return 0;
}

