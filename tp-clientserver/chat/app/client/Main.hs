{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.IO as T
import qualified Network.WebSockets as WS

main :: IO ()
main = WS.runClient "0.0.0.0" 9000 "" handleConn

handleConn :: WS.Connection -> IO ()
handleConn conn = do
    msgFromSrv <- WS.receiveDataMessage conn
    T.putStrLn $ WS.fromDataMessage msgFromSrv
    handleConn conn

handleInput :: Var -> IO ()
handleInput var = do
    msg <- T.getLine
    modifyMVar_ var  $ \net -> do
        mapInNet (\c -> WS.sendTextData c msg) net
        return net
    handleInput var
