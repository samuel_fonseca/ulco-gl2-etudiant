#include <math.h>

#define PI 3.14159265

double sinus(double n, double p, int x) {
    double result;
    result = sin(2*PI*(n*x+p));
    return result;
}

#include <emscripten/bind.h>

EMSCRIPTEN_BINDINGS(sinus) {
    emscripten::function("sinus", &sinus);
}